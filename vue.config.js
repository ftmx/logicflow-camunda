module.exports = {
  publicPath: './',
  configureWebpack: {
    devtool: "source-map"
  },
  productionSourceMap: true,
}