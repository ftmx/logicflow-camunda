# logicflow_vue_demo

> Logic-flow 的vue应用实例  
> logic Flow 详细内容参照[【文档】](http://logic-flow.org/)
### 功能
实例中包含的功能实现：
- 画布
- 快捷操作
- 节点面板
- 节点自定义
- 节点菜单
- 图形下载
- camunda工作流自定义

画布：src/components/LF.vue  
节点自定义： src/components/registerNode  
节点面板：src/components/NodePanel.vue

camunda:src/components/myFlow
![输入图片说明](src/logic-flow.png)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
