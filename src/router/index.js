import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import LF from '@/components/LF'
import TurboAdpter from '@/components/TurboAdpter'
import Test from '@/components/Test'
import MyFlow from '@/components/logicflowBpmn'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/lf',
      name: 'LF',
      component: LF
    },
    {
      path: '/test',
      name: 'Test',
      component: Test
    },
    {
      path: '/',
      name: 'MyFlow',
      component: MyFlow
    },
    {
      path: '/TurboAdpter',
      name: 'TurboAdpter',
      component: TurboAdpter
    }
  ]
})
