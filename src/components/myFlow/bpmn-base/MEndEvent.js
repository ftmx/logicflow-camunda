import {
    EndEventModel,EndEventView
} from "@logicflow/extension";
import {getBpmnId} from '../bpmn-adapter/bpmnIds'
class MEndEventModel extends EndEventModel {
    constructor(data, graphModel) {
        if (!data.id) {
            data.id = `EndEvent_${getBpmnId()}`;
        }
        super(data, graphModel);
    }
}

class MEndEventView extends EndEventView {}

export default {
    type: "bpmn:endEvent",
    view: MEndEventView,
    model: MEndEventModel
};