import {
    StartEventModel,StartEventView
} from "@logicflow/extension";
import {getBpmnId} from '../bpmn-adapter/bpmnIds'
class MStartEventModel extends StartEventModel {
    constructor(data, graphModel) {
        if (!data.id) {
            data.id = `StartEvent_${getBpmnId()}`;
        }
        super(data, graphModel);
    }
}

class MStartEventView extends StartEventView {}

export default {
    type: "bpmn:startEvent",
    view: MStartEventView,
    model: MStartEventModel
};