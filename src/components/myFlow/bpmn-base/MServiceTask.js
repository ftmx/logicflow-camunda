import {
    ServiceTaskModel,ServiceTaskView
} from "@logicflow/extension";

class MServiceTaskModel extends ServiceTaskModel {}

class MServiceTaskView extends ServiceTaskView {}

export default {
    type: "bpmn:serviceTask",
    view: MServiceTaskView,
    model: MServiceTaskModel
};