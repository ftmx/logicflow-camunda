import {
    ExclusiveGatewayModel,ExclusiveGatewayView
} from "@logicflow/extension";

class MExclusiveGatewayModel extends ExclusiveGatewayModel {}

class MExclusiveGatewayView extends ExclusiveGatewayView {}

export default {
    type: "bpmn:exclusiveGateway",
    view: MExclusiveGatewayView,
    model: MExclusiveGatewayModel
};