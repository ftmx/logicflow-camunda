import {
    SequenceFlowModel,SequenceFlowView
} from "@logicflow/extension";
import {getBpmnId} from "../bpmn-adapter/bpmnIds";

class MSequenceFlowModel extends SequenceFlowModel {
    constructor(data, graphModel) {
        if (!data.id) {
            data.id = `SequenceFlow_${getBpmnId()}`;
        }
        super(data, graphModel);
    }
}

class MSequenceFlowView extends SequenceFlowView {}

export default {
    type: "bpmn:sequenceFlow",
    view: MSequenceFlowView,
    model: MSequenceFlowModel
};