import {
    UserTaskModel,UserTaskView
} from "@logicflow/extension";
import {getBpmnId} from '../bpmn-adapter/bpmnIds'

class MUserTaskModel extends UserTaskModel {
    constructor(data, graphModel) {
        if (!data.id) {
            data.id = `UserTask_${getBpmnId()}`;
        }
        super(data, graphModel);
    }

}

class MUserTaskView extends UserTaskView {}

export default {
    type: "bpmn:userTask",
    view: MUserTaskView,
    model: MUserTaskModel
};